<?php
/**
 * Created by PhpStorm.
 * User: Dennis
 * Date: 8/15/2019
 * Time: 9:36 AM
 */

namespace Modules\System\Traits;


use Modules\System\Repositories\FileRepository;

trait SystemRepositoryTrait
{
    /**
     * @return self
     */
    public static function init()
    {
        return app(self::class);
    }

    /**
     * @param string $request_file_key
     * @return array
     */
    protected function autoSaveFileFromRequest($request_file_key = 'image')
    {
        if (request()->hasFile($request_file_key)) {
            $uploaded_image = request()->file($request_file_key);
            $filename = time() . '_' . rand(0, 10) . '_.' . $uploaded_image->clientExtension();

            $path = FileRepository::init()->saveFileAs($uploaded_image, $filename);

            return [
                'url' => url('/storage' . $path),
                'filename' => $filename,
                'path' => $path
            ];
        }

        return null;
    }

}