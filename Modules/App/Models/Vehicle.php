<?php

namespace Modules\App\Models;

use Illuminate\Database\Eloquent\Model;
use Modules\Qrcodes\Traits\QrCodeAbleTrait;

class Vehicle extends Model
{
    use QrCodeAbleTrait;

    protected $table = 'vehicles';

    protected $guarded = ['id'];

    public function vehicleUser()
    {
        return $this->belongsTo(VehicleUser::class,'vehicle_user_id');
    }

    public function licenses()
    {
        return $this->hasMany(VehicleLicense::class,'vehicle_id');
    }

    public function roadWorthinesses()
    {
        return $this->hasMany(RoadWorthiness::class,'vehicle_id');
    }
}
