<?php

namespace Modules\App\Models;

use Illuminate\Database\Eloquent\Model;
use Modules\System\Traits\PersonTrait;

class VehicleUser extends Model
{
    use PersonTrait;

    protected $table = 'vehicle_users';

    protected $guarded = ['id'];

    public function vehicles()
    {
        return $this->hasMany(Vehicle::class,'vehicle_user_id');
    }
}
