<?php

namespace Modules\App\Models;

use Illuminate\Database\Eloquent\Model;

class RoadWorthiness extends Model
{
    protected $table = 'road_worthinesses';

    protected $guarded = ['id'];

    protected $dates = [
        'issued_at',
        'expires_at'
    ];

    public function vehicle()
    {
        return $this->belongsTo(Vehicle::class,'vehicle_id');
    }


    public function isExpired()
    {
        return now()->isAfter($this->expires_at);
    }
}
