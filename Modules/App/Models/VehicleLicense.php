<?php

namespace Modules\App\Models;

use Illuminate\Database\Eloquent\Model;
use Modules\Qrcodes\Traits\QrCodeAbleTrait;

class VehicleLicense extends Model
{
    use QrCodeAbleTrait;

    protected $table = 'vehicle_licenses';

    protected $guarded = ['id'];

    protected $dates = [
        'issued_at',
        'expires_at'
    ];

    public function vehicle()
    {
        return $this->belongsTo(Vehicle::class,'vehicle_id');
    }

    public function isExpired()
    {
        return now()->isAfter($this->expires_at);
    }


}
