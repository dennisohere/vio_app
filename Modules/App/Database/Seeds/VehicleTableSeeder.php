<?php

namespace Modules\App\Database\Seeds;

use Illuminate\Database\Seeder;
use Modules\App\Models\VehicleUser;
use Modules\App\Repositories\VehicleRepository;
use Faker\Generator as Faker;

class VehicleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $number_to_generate = 70;
        /** @var Faker $faker */
        $faker = app(Faker::class);

        $vehicleUsers = VehicleUser::all();

        for ($i = 1; $i <= $number_to_generate; $i++){

            $vehicleUser = $vehicleUsers->random();

            VehicleRepository::init()->save($vehicleUser, [
                'make' => $faker->randomElement(['Hyundai', 'Toyota', 'Mercedes Benz', 'Lexus']),
                'type' => $faker->randomElement(['Car', 'Bus', 'Van']),
                'engineCapacity' => $faker->randomFloat(2, 1, 3),
            ]);
        }
    }
}
