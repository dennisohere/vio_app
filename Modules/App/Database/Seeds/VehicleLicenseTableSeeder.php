<?php

namespace Modules\App\Database\Seeds;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Faker\Generator as Faker;
use Modules\App\Models\Vehicle;
use Modules\App\Repositories\VehicleLicenseRepository;

class VehicleLicenseTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $number_to_generate = 70;
        /** @var Faker $faker */
        $faker = app(Faker::class);
        $vehicles = Vehicle::all();

        for ($i = 1; $i <= $number_to_generate; $i++){

            $vehicle = $vehicles->get($i) ?? $vehicles->random();

            $issued_date = $faker->dateTimeBetween('-5 years', '-2 years')->format('Y-m-d');

            VehicleLicenseRepository::init()->save($vehicle, [
                'issued' => $issued_date,
                'expires' => Carbon::createFromFormat('Y-m-d', $issued_date)->addYears(4)->toDate(),
            ]);
        }
    }
}
