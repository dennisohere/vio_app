<?php

namespace Modules\App\Database\Seeds;

use Illuminate\Database\Seeder;

class AppDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(VehicleUserTableSeeder::class);
        $this->call(VehicleTableSeeder::class);
        $this->call(VehicleLicenseTableSeeder::class);
        $this->call(RoadWorthinessTableSeeder::class);
    }
}
