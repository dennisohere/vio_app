<?php

namespace Modules\App\Database\Seeds;

use Illuminate\Database\Seeder;
use Modules\App\Repositories\VehicleUserRepository;
use Faker\Generator as Faker;

class VehicleUserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $names = [
            'Ajayi',
            'Alake',
            'Apara',
            'Olayinka',
            'Abiodun',
            'Abiola',
            'Adetola',
            'Ayotomiwa',
            'Babatunji',
            'Farayioluwa',
            'Gbadebo',

            'Adamu',
            'Aminu',
            'Aisha',
            'Danjuma',
            'Danlami',
            'Faidah',
            'Fatimah',
            'Faruq',
            'Habib',
            'Latifah',

            'Chibuikem',
            'Chidindu',
            'Chimamanda',
            'Chizutere',
            'Chukwuebuka',
            'Ekene',
            'Kamsiyochukwu',
            'Onochie',
            'Afamefuna',
            'Akachukwu',
            'Amaechi',
            'Amandi',
            'Amara'
        ];

        $number_to_generate = 20;
        /** @var Faker $faker */
        $faker = app(Faker::class);

        for ($i = 1; $i <= $number_to_generate; $i++){
            VehicleUserRepository::init()->save([
                'firstName' => $faker->randomElement($names),
                'lastName' => $faker->randomElement($names),
                'dob' => $faker->date('Y-m-d', '-15 years'),
                'email' => $faker->safeEmail,
                'phone' => $faker->phoneNumber,
                'address' => $faker->address
            ]);
        }
    }
}
