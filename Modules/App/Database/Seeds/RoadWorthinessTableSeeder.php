<?php

namespace Modules\App\Database\Seeds;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Modules\App\Models\Vehicle;
use Modules\App\Models\VehiclePaper;
use Modules\App\Repositories\RoadWorthinessRepository;
use Faker\Generator as Faker;

class RoadWorthinessTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $number_to_generate = 65;
        /** @var Faker $faker */
        $faker = app(Faker::class);

        $vehicles = Vehicle::all();

        for ($i = 1; $i <= $number_to_generate; $i++){

            $vehicle = $vehicles->get($i) ?? $vehicles->random();

            $issued_date = $faker->dateTimeBetween('-3 years', '-2 years')->format('Y-m-d');

            RoadWorthinessRepository::init()->save($vehicle, [
                'issued' => $issued_date,
                'expires' => Carbon::createFromFormat('Y-m-d', $issued_date)->addYears(3)->toDate(),
            ]);
        }
    }
}
