<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehiclesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicles', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('vehicle_user_id');
            $table->foreign('vehicle_user_id')->references('id')->on('vehicle_users')->onDelete('cascade');

            $table->string('reg_no')->unique();
            $table->string('chasis_no')->unique();
            $table->string('make');
            $table->string('type')->default('Car');
            $table->double('engine_capacity')->default(0);

            $table->longText('metadata')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicles');
    }
}
