<?php
/**
 * Created by PhpStorm.
 * User: Dennis
 * Date: 9/12/2019
 * Time: 1:53 PM
 */

namespace Modules\App\Repositories;


use Modules\App\Models\VehicleUser;
use Modules\System\Traits\SystemRepositoryTrait;

class VehicleUserRepository
{

    use SystemRepositoryTrait;
    /**
     * @var VehicleUser
     */
    private $vehicleUser;


    /**
     * VehicleUserRepository constructor.
     * @param VehicleUser $vehicleUser
     */
    public function __construct(VehicleUser $vehicleUser)
    {
        $this->vehicleUser = $vehicleUser;
    }

    public function save(array $payload)
    {
        $edit = !!isset($payload['id']);

        $vehicle_user = $edit ? $this->getById($payload['id']) : $this->vehicleUser->newInstance();

        $vehicle_user->fill([
            'first_name' => $payload['firstName'],
            'last_name' => $payload['lastName'],
            'dob' => $payload['dob'],
            'email' => $payload['email'],
            'phone' => $payload['phone']
        ]);

        $vehicle_user->save();

        return $vehicle_user;
    }

    /**
     * @param $id
     * @return VehicleUser
     */
    public function getById($id)
    {
        return $this->vehicleUser->find($id);
    }
}