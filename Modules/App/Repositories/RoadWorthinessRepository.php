<?php
/**
 * Created by PhpStorm.
 * User: Dennis
 * Date: 9/12/2019
 * Time: 1:53 PM
 */

namespace Modules\App\Repositories;


use Illuminate\Support\Str;
use Modules\App\Models\RoadWorthiness;
use Modules\App\Models\Vehicle;
use Modules\App\Models\VehiclePaper;
use Modules\System\Traits\SystemRepositoryTrait;

class RoadWorthinessRepository
{

    use SystemRepositoryTrait;
    /**
     * @var RoadWorthiness
     */
    private $roadWorthiness;


    /**
     * RoadWorthinessRepository constructor.
     * @param RoadWorthiness $roadWorthiness
     */
    public function __construct(RoadWorthiness $roadWorthiness)
    {

        $this->roadWorthiness = $roadWorthiness;
    }

    /**
     * @param Vehicle $vehicle
     * @param array $payload
     * @return RoadWorthiness
     */
    public function save(Vehicle $vehicle, array $payload)
    {
        $edit = !!isset($payload['id']);

        $roadWorthiness = $edit ? $this->getById($payload['id']) : $this->roadWorthiness->newInstance();

        $roadWorthiness->fill([
            'vehicle_id' => $vehicle->id,
            'issued_at' => $payload['issued'],
            'expires_at' => $payload['expires']
        ]);

        $roadWorthiness->save();

        return $roadWorthiness;
    }

    /**
     * @param $id
     * @return RoadWorthiness
     */
    public function getById($id)
    {
        return $this->roadWorthiness->find($id);
    }
}