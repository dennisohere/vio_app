<?php
/**
 * Created by PhpStorm.
 * User: Dennis
 * Date: 9/12/2019
 * Time: 1:53 PM
 */

namespace Modules\App\Repositories;


use Modules\App\Models\Vehicle;
use Modules\App\Models\VehicleLicense;
use Modules\System\Traits\SystemRepositoryTrait;

class VehicleLicenseRepository
{

    use SystemRepositoryTrait;
    /**
     * @var VehicleLicense
     */
    private $vehicleLicense;


    /**
     * VehiclePaperRepository constructor.
     * @param VehicleLicense $vehicleLicense
     */
    public function __construct(VehicleLicense $vehicleLicense)
    {

        $this->vehicleLicense = $vehicleLicense;
    }

    /**
     * @param Vehicle $vehicle
     * @param array $payload
     * @return VehicleLicense
     */
    public function save(Vehicle $vehicle, array $payload)
    {
        $edit = !!isset($payload['id']);

        $vehicle_license = $edit ? $this->getById($payload['id']) : $this->vehicleLicense->newInstance();

        $vehicle_license->fill([
            'vehicle_id' => $vehicle->id,
            'issued_at' => $payload['issued'],
            'expires_at' => $payload['expires']
        ]);

        $vehicle_license->save();

        return $vehicle_license;
    }

    /**
     * @param $id
     * @return Vehicle
     */
    public function getById($id)
    {
        return $this->vehicleLicense->find($id);
    }
}