<?php
/**
 * Created by PhpStorm.
 * User: Dennis
 * Date: 9/12/2019
 * Time: 1:53 PM
 */

namespace Modules\App\Repositories;


use Illuminate\Support\Str;
use Modules\App\Models\Vehicle;
use Modules\App\Models\VehicleUser;
use Modules\System\Traits\SystemRepositoryTrait;

class VehicleRepository
{

    use SystemRepositoryTrait;

    /**
     * @var Vehicle
     */
    private $vehicle;


    /**
     * VehicleRepository constructor.
     * @param Vehicle $vehicle
     */
    public function __construct(Vehicle $vehicle)
    {

        $this->vehicle = $vehicle;
    }

    public function save(VehicleUser $user, array $payload)
    {
        $edit = !!isset($payload['id']);

        $vehicle = $edit ? $this->getById($payload['id']) : $this->vehicle->newInstance();

        $vehicle->fill([
            'reg_no' => Str::random(),
            'chasis_no' => Str::random(),
            'make' => $payload['make'],
            'engine_capacity' => $payload['engineCapacity'],
            'type' => $payload['type'] ?? null,
            'vehicle_user_id' => $user->id
        ]);

        $vehicle->save();

        return $vehicle;
    }

    /**
     * @param $id
     * @return Vehicle
     */
    public function getById($id)
    {
        return $this->vehicle->find($id);
    }
}