<?php

namespace Modules\App\Providers;

use Caffeinated\Modules\Support\ServiceProvider;
use Modules\App\Models\Vehicle;
use Modules\App\Observers\VehicleObserver;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the module services.
     *
     * @return void
     */
    public function boot()
    {
        Vehicle::observe(VehicleObserver::class);

        $this->loadTranslationsFrom(module_path('app', 'Resources/Lang', 'app'), 'app');
        $this->loadViewsFrom(module_path('app', 'Resources/Views', 'app'), 'app');
        $this->loadMigrationsFrom(module_path('app', 'Database/Migrations', 'app'), 'app');
        $this->loadConfigsFrom(module_path('app', 'Config', 'app'));
        $this->loadFactoriesFrom(module_path('app', 'Database/Factories', 'app'));
    }

    /**
     * Register the module services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }
}
