<?php
/**
 * Created by PhpStorm.
 * User: Dennis
 * Date: 9/12/2019
 * Time: 2:38 PM
 */

namespace Modules\App\Http\Resources;


use Illuminate\Http\Resources\Json\Resource;
use Modules\App\Models\VehicleLicense;
use Modules\App\Models\VehiclePaper;

class VehicleLicenseResource extends Resource
{
    public function toArray($request)
    {
        /** @var VehicleLicense $vehicleLicense */
        $vehicleLicense = $this;

        return [
            'id' => $vehicleLicense->id,
            'isExpired' => $vehicleLicense->isExpired(),
            'issuedOn' => $vehicleLicense->issued_at,
            'expiresOn' => $vehicleLicense->expires_at,
        ];
    }


}