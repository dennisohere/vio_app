<?php
/**
 * Created by PhpStorm.
 * User: Dennis
 * Date: 9/12/2019
 * Time: 2:38 PM
 */

namespace Modules\App\Http\Resources;


use Illuminate\Http\Resources\Json\Resource;
use Modules\App\Models\RoadWorthiness;
use Modules\App\Models\VehiclePaper;

class RoadWorthinessResource extends Resource
{
    public function toArray($request)
    {
        /** @var RoadWorthiness $roadWorthiness */
        $roadWorthiness = $this;

        return [
            'id' => $roadWorthiness->id,
            'isExpired' => $roadWorthiness->isExpired(),
            'issuedOn' => $roadWorthiness->issued_at,
            'expiresOn' => $roadWorthiness->expires_at,
        ];
    }


}