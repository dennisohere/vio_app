<?php
/**
 * Created by PhpStorm.
 * User: Dennis
 * Date: 9/12/2019
 * Time: 2:41 PM
 */

namespace Modules\App\Http\Resources;


use Illuminate\Http\Resources\Json\Resource;
use Modules\App\Models\VehicleUser;

class VehicleUserResource extends Resource
{
    public function toArray($request)
    {
        /** @var VehicleUser $vehicle_user */
        $vehicle_user = $this;

        return [
            'id' => $vehicle_user->id,
            'fullName' => $vehicle_user->getFullName(),
            'email' => $vehicle_user->email,
            'phone' => $vehicle_user->phone,
            'dob' => $vehicle_user->dob,
        ];
    }


}