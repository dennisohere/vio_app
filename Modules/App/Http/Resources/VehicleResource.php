<?php
/**
 * Created by PhpStorm.
 * User: Dennis
 * Date: 9/12/2019
 * Time: 2:39 PM
 */

namespace Modules\App\Http\Resources;


use Illuminate\Http\Resources\Json\Resource;
use Modules\App\Models\Vehicle;

class VehicleResource extends Resource
{
    public function toArray($request)
    {
        /** @var Vehicle $vehicle */
        $vehicle = $this;

        return [
            'id' => $vehicle->id,
            'make' => $vehicle->make,
            'engineCapacity' => $vehicle->engine_capacity,
            'regNo' => $vehicle->reg_no,
            'chasisNo' => $vehicle->chasis_no,
            'user' => new VehicleUserResource($vehicle->vehicleUser),
            'licenses' => VehicleLicenseResource::collection($vehicle->licenses()->orderBy('expires_at', 'desc')->latest()->get()),
            'roadWorthinesses' => RoadWorthinessResource::collection($vehicle->roadWorthinesses()->orderBy('expires_at', 'desc')->latest()->get()),
        ];
    }


}