<!-- Navigation -->
<ul class="navbar-nav">
    <li class="nav-item active" >
        <a class=" nav-link active " href=""> <i class="ni ni-tv-2 text-primary"></i> Dashboard
        </a>
    </li>

    <li class="nav-item" >
        <a class=" nav-link " href=""> <i class="ni ni-books text-primary"></i> Vehicle Registrations</a>
    </li>

    <li class="nav-item" >
        <a class=" nav-link " href="{{route('qrcodes.index')}}"> <i class="ni ni-books text-primary"></i> Qr-Codes</a>
    </li>
</ul>
