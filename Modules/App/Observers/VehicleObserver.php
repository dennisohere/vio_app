<?php
/**
 * Created by PhpStorm.
 * User: Dennis
 * Date: 9/12/2019
 * Time: 2:30 PM
 */

namespace Modules\App\Observers;


use Modules\App\Models\Vehicle;
use Modules\Qrcodes\Repositories\QrcodeRepository;

class VehicleObserver
{

    public function created(Vehicle $vehicle)
    {

        // todo assign qr code to vehicle paper

        $qrCode = QrcodeRepository::init()->getAvailableCode();

        $qrCode->qrCodeAble()->associate($vehicle);
        $qrCode->update([
           'is_active' => true
        ]);
    }

}