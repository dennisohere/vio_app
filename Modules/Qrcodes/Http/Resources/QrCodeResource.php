<?php
/**
 * Created by PhpStorm.
 * User: Dennis
 * Date: 9/9/2019
 * Time: 12:59 PM
 */

namespace Modules\Qrcodes\Http\Resources;


use Illuminate\Http\Resources\Json\Resource;
use Modules\App\Http\Resources\VehiclePaperResource;
use Modules\App\Http\Resources\VehicleResource;
use Modules\App\Models\Vehicle;
use Modules\App\Models\VehiclePaper;
use Modules\Qrcodes\Models\QrCode;

class QrCodeResource extends Resource
{
    public function toArray($request)
    {
        /** @var QrCode $qrcode */
        $qrcode = $this;

        $data = [
            'id' => $qrcode->id,
            'code' => $qrcode->code,
            'active' => !!$qrcode->is_active,
        ];

        $qr_code_able = $qrcode->qrCodeAble;

        if($qr_code_able instanceof Vehicle){
            $data['vehicle'] = new VehicleResource($qr_code_able);
        }

        return $data;
    }


}