<?php

namespace Modules\Qrcodes\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Modules\Qrcodes\Models\QrCode;
use Modules\Qrcodes\Repositories\QrcodeRepository;

class QrcodeController extends Controller
{
    public function index()
    {
        $codes = QrcodeRepository::init()->getAllQrCodes();

        return view('qrcodes::qrcodes', compact('codes'));
    }

    public function details(QrCode $qrCode)
    {

        return view('qrcodes::code-details', compact('qrCode'));

    }
}
