<?php

namespace Modules\Qrcodes\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Modules\Qrcodes\Http\Resources\QrCodeResource;
use Modules\Qrcodes\Models\QrCode;
use Modules\Qrcodes\Repositories\QrcodeRepository;

class QrcodesController extends Controller
{
    public function getAllQrCodes(Request $request)
    {

        $codes = QrcodeRepository::init()->getAllQrCodes();

        return QrCodeResource::collection($codes);

    }

    public function verify($code)
    {
        $qr = QrcodeRepository::init()->getQrCodeByCode($code);

        return new QrCodeResource($qr);
    }
}
