<?php

namespace Modules\Qrcodes\Providers;

use Caffeinated\Modules\Support\ServiceProvider;
use Modules\Qrcodes\Models\QrCode;
use Modules\Qrcodes\Observers\QrCodeObserver;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the module services.
     *
     * @return void
     */
    public function boot()
    {
        QrCode::observe(QrCodeObserver::class);

        $this->loadTranslationsFrom(module_path('qrcodes', 'Resources/Lang', 'app'), 'qrcodes');
        $this->loadViewsFrom(module_path('qrcodes', 'Resources/Views', 'app'), 'qrcodes');
        $this->loadMigrationsFrom(module_path('qrcodes', 'Database/Migrations', 'app'), 'qrcodes');
        $this->loadConfigsFrom(module_path('qrcodes', 'Config', 'app'));
        $this->loadFactoriesFrom(module_path('qrcodes', 'Database/Factories', 'app'));
    }

    /**
     * Register the module services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }
}
