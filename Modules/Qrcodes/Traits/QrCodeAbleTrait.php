<?php
/**
 * Created by PhpStorm.
 * User: Dennis
 * Date: 9/11/2019
 * Time: 7:04 PM
 */

namespace Modules\Qrcodes\Traits;


use Modules\Qrcodes\Models\QrCode;

trait QrCodeAbleTrait
{
    public function qrCode()
    {
        return $this->morphOne(QrCode::class,'qr_code_able');
    }

}