@extends('app::layout.app')

@section('app')

    <div class="row">

        <div class="col-sm-12">
            <div class="card shadow">
                <div class="card-header border-0">
                    <div class="row align-items-center">
                        <div class="col">
                            <h3 class="mb-0">Generated Qr-Codes</h3>
                        </div>
                        <div class="col text-right">
                            {{--<button class="btn btn-sm btn-primary"
                                    data-target="#modalNew" data-toggle="modal">
                                Add Menu
                            </button>--}}
                        </div>
                    </div>
                </div>

                @if($codes->count() > 0)

                    <div class="table-responsive">
                        <!-- Projects table -->
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                            <tr>
                                <th scope="col">Code</th>
                                <th scope="col">Status</th>
                                <th scope="col"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($codes as $code)
                                <tr>
                                    <th scope="row">
                                        {{$code->code}}
                                    </th>

                                    <td>
                                        <span class="badge badge-{{$code->is_active ? 'success' : 'warning'}}">
                                            {{$code->is_active ? 'Active' : 'Unassigned'}}
                                        </span>
                                    </td>

                                    <td>
                                        <a href="{{route('qrcodes.details', ['qrCode' => $code->code])}}"
                                           onclick="var popup_window = window.open(this.href, 'mywin','location=0,toolbar=0,menubar=0,scrollbars=1,height=600,width=1000');
                           return false;"
                                           class="btn btn-sm btn-info">
                                            See details
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                @else

                    <div class="card-body">
                        <p class="card-title text-center text-warning">No Data Available.</p>
                    </div>

                @endif

            </div>
        </div>

    </div>

@endsection


@push('scripts')



@endpush