@extends('app::layout.base')

@section('base')

    <div class="container py-5">
        <div class="row">

            <div class="col-sm-12">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col">
                                <h3 class="mb-0">Code: {{$qrCode->code}}</h3>
                            </div>
                            <div class="col text-right">
                                {{--<button class="btn btn-sm btn-primary"
                                        data-target="#modalNew" data-toggle="modal">
                                    Add Menu
                                </button>--}}
                            </div>
                        </div>
                    </div>

                    <div class="card-body">

                        <img src="{{$qrCode->getQrCodeImagePath()}}" alt="{{config('app.name')}}">

                    </div>

                </div>
            </div>

        </div>
    </div>

@endsection


@push('scripts')



@endpush