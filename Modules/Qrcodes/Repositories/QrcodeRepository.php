<?php
/**
 * Created by PhpStorm.
 * User: Dennis
 * Date: 9/9/2019
 * Time: 12:55 PM
 */

namespace Modules\Qrcodes\Repositories;


use Modules\Qrcodes\Models\QrCode;
use Modules\System\Traits\SystemRepositoryTrait;

class QrcodeRepository
{
    use SystemRepositoryTrait;
    /**
     * @var QrCode
     */
    private $qrCode;


    /**
     * QrcodeRepository constructor.
     * @param QrCode $qrCode
     */
    public function __construct(QrCode $qrCode)
    {
        $this->qrCode = $qrCode;
    }

    public function getAllQrCodes($limit = null)
    {
        $query = $this->qrCode->newQuery()
            ->orderBy('is_active', 'desc')
            ->latest()
        ;

        if(!$limit) return $query->get();

        return $query->paginate($limit);
    }

    public function createCode($code)
    {
        return $this->qrCode->create([
           'code' => $code
        ]);
    }

    /**
     * @param $code
     * @return QrCode | null
     */
    public function getQrCodeByCode($code)
    {
        return $this->qrCode->where('code', $code)->first();
    }


    /**
     * @return QrCode
     */
    public function getAvailableCode()
    {
        return $this->qrCode->where('is_active', false)->first();
    }
}