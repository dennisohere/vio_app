<?php

namespace Modules\Qrcodes\Database\Seeds;

use Illuminate\Database\Seeder;
use Modules\Qrcodes\Jobs\CreateQrCode;

class QrcodesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        if(file_exists(storage_path('app/public/qr_codes')))
        deleteDir(storage_path('app/public/qr_codes'));

        $generate_number = 100;

        for ($i = 1; $i <= $generate_number; $i++){
            dispatch_now(new CreateQrCode());
        }
    }
}
