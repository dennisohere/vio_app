<?php
/**
 * Created by PhpStorm.
 * User: Dennis
 * Date: 9/11/2019
 * Time: 7:32 PM
 */

namespace Modules\Qrcodes\Observers;


use Modules\Qrcodes\Models\QrCode;

class QrCodeObserver
{
    public function created(QrCode $qrCode)
    {


        // todo: generate qr code image

        if(!file_exists(public_path('qr_codes'))){
            mkdir(public_path('qr_codes'));
        }

        $qr_image = $qrCode->code . '.png';
        \SimpleSoftwareIO\QrCode\Facades\QrCode::format('png')
            ->size(300)
            ->generate($qrCode->getApiLink(), public_path('qr_codes/' . $qr_image));

        $qrCode->update([
            'qr_image' => $qr_image
        ]);

    }

}