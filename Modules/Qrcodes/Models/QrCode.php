<?php

namespace Modules\Qrcodes\Models;

use Illuminate\Database\Eloquent\Model;

class QrCode extends Model
{
    protected $table = 'qr_codes';

    protected $guarded = ['id'];


    public function getRouteKeyName()
    {
        return 'code';
    }

    public function qrCodeAble()
    {
        return $this->morphTo('qr_code_able');
    }

    public function getApiLink()
    {
        return route('api.v1.qrcodes.verify', ['code' => $this->code]);
    }

    public function getQrCodeImagePath()
    {
        return asset('qr_codes/' . $this->qr_image);
    }
}
