<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::prefix('qrcodes')->name('qrcodes.')->group(function () {
    Route::get('/', 'QrcodeController@index')->name('index');
    Route::get('{qrCode}/details', 'QrcodeController@details')->name('details');
});
