<?php

use Illuminate\Support\Facades\Route;

Route::name('v1.qrcodes.')->prefix('qrcodes')->namespace('Api')->group(function(){
//    Route::post('login', 'Api\LoginController@login')->name('login');
    Route::get('index', 'QrcodesController@index')->name('index');
    Route::get('{code}/verify', 'QrcodesController@verify')->name('verify');
});