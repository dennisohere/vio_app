<?php
/**
 * Created by PhpStorm.
 * User: Dennis
 * Date: 9/11/2019
 * Time: 7:26 PM
 */

namespace Modules\Qrcodes\Jobs;


use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Str;
use Modules\Qrcodes\Repositories\QrcodeRepository;

class CreateQrCode implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable;
    /**
     * @var null|string
     */
    private $code;


    /**
     * CreateQrCode constructor.
     * @param string $code
     */
    public function __construct($code = null)
    {
        $this->code = $code;
    }

    public function handle()
    {
        if(!$this->code) $this->code = Str::random();

        QrcodeRepository::init()->createCode($this->code);

    }

}